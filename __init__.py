from flask import Flask, request, render_template, make_response, \
                   redirect, url_for, flash
from werkzeug.utils import secure_filename
import string
import secrets
import os
import json
from flask_autoindex import AutoIndex

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

path_sens_file = os.environ.get('WEBAPP_USERS')
path_folder = os.environ.get('WEBAPP_FOLDER')

app = Flask(__name__)
app.secret_key = 'RWwrr674ffhyf53'

files_index = AutoIndex(app, browse_root=path_folder,  add_url_rules=False)

msg = ''


def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


# reading users
try:
    with open(str(path_sens_file)) as json_file:
        users = json.load(json_file)
except OSError as err:
    print("OS error: {0}".format(err))
    exit()
else:
    for user in users.keys():
        alphabet = string.ascii_letters + string.digits
        key = ''.join(secrets.choice(alphabet) for i in range(16))
        users[user]['key'] = key


def user_validate():
    user = request.cookies.get('user')
    key = request.cookies.get('key')
    if user in users:
        if users[user]['key'] == key:
            return user
    return None


@app.route('/')
@app.route('/index')
def index():
    global msg
    msg1 = msg
    msg = ''
    user = user_validate()
    if user is not None:
        return render_template('index.html', type='protected', msg=msg1,
                               name=user)
    return render_template('index.html', type='login', msg=msg1)


@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        user = request.form['name']
        password = request.form['password']
        if user in users:
            if users[user]['secret'] == password:
                alphabet = string.ascii_letters + string.digits
                key = ''.join(secrets.choice(alphabet) for i in range(16))
                users[user]['key'] = key
                resp = make_response(render_template('index.html',
                                                     type='protected',
                                                     name=user))
                resp.set_cookie('user', user)
                resp.set_cookie('key', key)
                return resp
    global msg
    msg = 'Bad login'
    return redirect(url_for('index'))


@app.route('/logout')
def logout():
    user = request.cookies.get('user')
    key = request.cookies.get('key')
    if user in users:
        if users[user]['key'] == key:
            users[user]['key'] = ''
    global msg
    msg = 'Logout'
    return redirect(url_for('index'))


@app.route('/files', methods=['GET', 'POST'])
@app.route('/files/<path:path>')
def autoindex(path=path_folder):
    global msg
    user = user_validate()
    if user is not None:
        if os.path.exists(path_folder):
            return files_index.render_autoindex(path)
        else:
            msg = 'directory ' + path + 'unavailable'
    else:
        msg = 'You had no permission'
    return redirect(url_for('index'))


@app.route('/files/')
def back():
    return redirect(url_for('index'))


# upload file
@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            global msg
            msg = 'You need login'
            user = user_validate()
            if user is not None:
                if os.path.exists(path_folder):
                    try:
                        file.save(os.path.join(path_folder, filename))
                    except OSError as err:
                        msg = "OS error: " + format(err)
                    else:
                        msg = filename + ' uploaded'
                    return redirect(url_for('index'))
                else:
                    msg = user + ' has no directory ' + path_folder
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
